import arrow
import datetime
import uuid as original_uuid
import random


# string utils
def uuid():
    return str(original_uuid.uuid4()).replace('-', '')


def random_code(size):
    return ''.join(
        str(random.randrange(0, 9))
        for i in xrange(size))


# date time utils
def now():
    return datetime.datetime.utcnow()


def now_delta(**kwargs):
    return now() + datetime.timedelta(**kwargs)


def avoid_weekend(start_time):
    weekday = start_time.weekday()
    if weekday == 5:  # saturday
        return start_time + datetime.timedelta(days=2)
    elif weekday == 6:  # sunday
        return start_time + datetime.timedelta(days=1)
    return start_time


def convert_date_time_between_timezones(date_time, from_tz, to_tz):
    return arrow.get(date_time, from_tz).to(
        to_tz).datetime.replace(tzinfo=None)


def convert_from_utc(date_time, tz):
    if date_time is None:
        return None
    return convert_date_time_between_timezones(date_time, 'UTC', tz)


def convert_to_utc(date_time, tz):
    return convert_date_time_between_timezones(date_time, tz, 'UTC')
