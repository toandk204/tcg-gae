from google.appengine.api import memcache


class Mutex(object):

    def __init__(self, name, time=30):
        """Create  mutex object using memcache

        Args:
          name: name of key, this will be key of memcache
          time: Optional expiration time, either relative number of seconds
            from current time (up to 1 month), or an absolute Unix epoch time.
            By default, items never expire, though items may be evicted due to
            memory pressure.  Float values will be rounded up to the nearest
            whole second.
            Default value is 30 seconds
        """
        self.name = name
        self.time = time or 30
        self.client = memcache.Client()

    def initial(self):
        self.client.add(self.name, 0,  time=self.time)

    def acquired(self):
        return self.client.incr(self.name) == 1

    def release(self):
        return self.client.decr(self.name)
