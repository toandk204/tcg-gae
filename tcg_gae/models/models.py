from google.appengine.ext import ndb

from mixins import IdModelMixin, MetaModelMixin


class MetaModel(ndb.model.MetaModel):

    def __init__(cls, name, bases, classdict):
        super(MetaModel, cls).__init__(name, bases, classdict)
        cls._add_manager()
        cls._add_reference_properties()
        cls._find_parent_fields()


class BaseModel(IdModelMixin, MetaModelMixin, ndb.Model):

    __metaclass__ = MetaModel

    @classmethod
    def create(cls, enable_hook=False, **kwargs):
        instance = cls(**kwargs)
        instance.put()
        return instance

    def __init__(self, **kwargs):
        if self._parent_field in kwargs:
            kwargs['parent'] = kwargs[self._parent_field].key
        elif '%s_key' % self._parent_field in kwargs:
            kwargs['parent'] = kwargs['%s_key' % self._parent_field]
        super(BaseModel, self).__init__(**kwargs)
        if hasattr(self, 'initialize'):
            self.initialize()

    def _validate_attributes(self, kwargs):
        _kwargs = kwargs.copy()
        for name, value in kwargs.iteritems():
            reference_key = name + '_key'
            if reference_key in self.reference_properties:
                prop = self.reference_properties[reference_key]
                is_subclass = issubclass(value.__class__, Model)
                is_correct_kind = prop._kind is None or prop._kind == value.__class__.__name__  # noqa
                if value is None or (is_subclass and is_correct_kind):
                    setattr(self, name, value)
                    _kwargs.pop(name)
                    _kwargs[reference_key] = value.key if value else None
                    continue
                else:  # pragma: no cover
                    message = 'Could not set %s to not kind of %s' % (
                        name, prop._kind.__class__.__name__)
                    raise Exception(message)
        return _kwargs

    def _set_attributes(self, kwds):
        _kwds = self._validate_attributes(kwds)
        super(BaseModel, self)._set_attributes(_kwds)

    def save(self):
        self.put()

    def update(self, **kwargs):
        data = self._validate_attributes(kwargs)
        self._old_data = {}
        self._new_data = {}
        for k, v in data.iteritems():
            old_value = getattr(self, k, None)
            if old_value != v:
                self._new_data[k] = v
                self._old_data[k] = old_value
        if self._new_data:
            self._set_attributes(self._new_data)
        if self.key:
            self.put()
        if self._new_data:
            return True

    def __getattr__(self, name):
        reference_key = name + '_key'
        if reference_key in self.reference_properties:
            key = getattr(self, reference_key)
            if key is not None:
                value = key.get()
                setattr(self, name, value)
                return value
            else:  # pragma: no cover
                return None
        else:
            message = '%s object has no attribute %s' % (
                self.__class__.__name__, name)
            raise AttributeError(message)


class Model(BaseModel):

    updated_at = ndb.DateTimeProperty(auto_now=True)
    created_at = ndb.DateTimeProperty(auto_now_add=True)
