class Manager(object):

    def __init__(self, cls):
        self.model = cls
        self.query = cls.query

    def filter(self, **kwargs):
        query = self.query()
        if self.model._parent_field:
            if self.model._parent_field in kwargs:
                ancestor = kwargs[self.model._parent_field].key
                query = self.query(ancestor=ancestor)
            elif '%s_key' % self.model._parent_field in kwargs:
                ancestor = kwargs['%s_key' % self.model._parent_field]
                query = self.query(ancestor=ancestor)

        for key, value in kwargs.iteritems():
            if key in self.model._properties:
                prop = self.model._properties[key]
                query = query.filter(prop._comparison('=', value))
            elif (key + '_key') in self.model.reference_properties:
                key = key + '_key'
                prop = self.model._properties[key]
                key_value = value.key if value else None
                query = query.filter(prop._comparison('=', key_value))
        return query
