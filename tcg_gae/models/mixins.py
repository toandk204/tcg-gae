from google.appengine.ext import ndb

from manager import Manager


class MetaModelMixin(object):

    @classmethod
    def _add_manager(cls):
        meta = getattr(cls, 'Meta', None)
        manager_class = getattr(meta, 'manager', Manager)
        if not issubclass(manager_class, Manager):  # pragma: no cover
            raise Exception(
                'Manager of class %s need to '
                'be subclass of Manager' % cls.__name__)
        cls.objects = manager_class(cls)

    @classmethod
    def _add_reference_properties(cls):
        result = {}
        for key, value in cls._properties.iteritems():
            if isinstance(value, ndb.KeyProperty):
                result[key] = value
        cls.reference_properties = result

    @classmethod
    def _find_parent_fields(cls):
        meta = getattr(cls, 'Meta', None)
        cls._parent_field = getattr(meta, 'parent', None)


class IdModelMixin(object):

    @property
    def safe_id(self):
        return self.key.urlsafe() if self.key else None

    @property
    def id(self):
        return self.key.id() if self.key else None
