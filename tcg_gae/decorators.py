def lazy(func):
    """
    This function used to cached a property of class

    E.g:

    class SomeService(object):

        def __init__(self, key):
            self.key = key

        @lazy
        def object(self):
            return self.key.get()

    s = SomeService(key)
    s.object  # this call self.key.get() at the first time, and cached it
    s.object  # this time, s.object is cached, so it will not be called again
    """
    attr_name = '_lazy_%s' % func.__name__

    @property
    def wrapper(self):
        if not hasattr(self, attr_name):
            setattr(self, attr_name, func(self))
        return getattr(self, attr_name)

    @wrapper.setter
    def wrapper(self, value):
        setattr(self, attr_name, value)

    return wrapper
