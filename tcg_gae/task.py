from functools import wraps

from google.appengine.ext import deferred

from mutex import Mutex


def merge(dict1, dict2):
    data = {}
    data.update(dict1)
    data.update(dict2)
    return data


class task(object):
    """
    This decorator help to create delay task easily

    Parameters could be:
    _countdown, _eta, _headers, _name, _target, _transactional, _url,
    _retry_options, _queue

    See the  appengine task queue documentation for details.
    https://cloud.google.com/appengine/docs/python/taskqueue/tasks

    This decorator could apply for both function and class. If class want to
    apply this decorator, class must define function `execute`.
    Please refer to `class_execute` function to see how it is implemented
    """

    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def class_execute(self, klass, *args, **kwargs):
        instance = klass(*args, **kwargs)
        return instance.execute()

    def func_execute(self, func, *args, **kwargs):
        return func(*args, **kwargs)

    def execute_delay(self, func, obj, func_args, func_kwargs):
        params = merge(self.kwargs, func_kwargs)
        deferred.defer(func, obj, *func_args, **params)

    def setup_delay_class(self, klass):
        @classmethod
        def delay(cls, *class_args, **class_kwargs):
            self.execute_delay(
                self.class_execute, klass, class_args, class_kwargs)
        klass.delay = delay

    def setup_delay_func(self, func):
        @wraps(func)
        def delay(*func_args, **func_kwargs):
            self.execute_delay(
                self.func_execute, func, func_args, func_kwargs)
        func.delay = delay

    def __call__(self, obj):
        if isinstance(obj, type):
            self.setup_delay_class(obj)
        else:
            self.setup_delay_func(obj)

        return obj


class single_task(task):

    def _execute(self, func_name, obj, *func_args, **func_kwargs):
        func = getattr(super(single_task, self), func_name)
        defer_func = getattr(self, func_name)

        mutex = Mutex(func_kwargs['key'])
        if mutex.acquired():
            func_kwargs.pop('key')
            func(obj, *func_args, **func_kwargs)
            mutex.release()
        else:
            func_kwargs['_countdown'] = self.kwargs.get('_countdown', 1)
            mutex.release()
            deferred.defer(defer_func, obj, *func_args, **func_kwargs)

    def class_execute(self, klass, *args, **kwargs):
        return self._execute('class_execute', klass, *args, **kwargs)

    def func_execute(self, func, *args, **kwargs):
        return self._execute('func_execute', func, *args, **kwargs)

    def execute_delay(self, func, obj, func_args, func_kwargs):
        mutex = Mutex(func_kwargs['key'])
        mutex.initial()
        super(single_task, self).execute_delay(
            func, obj, func_args, func_kwargs)
