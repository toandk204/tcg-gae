import warnings
import model_meta
import fields

from google.appengine.ext import ndb
from rest_framework.utils.field_mapping import (
    ClassLookupDict, get_nested_relation_kwargs)
from rest_framework.serializers import Serializer
from rest_framework.relations import *  # NOQA
from rest_framework.fields import *  # NOQA


class ModelSerializer(Serializer):  # pragma: no cover

    id = CharField(source='safe_id', read_only=True)

    serializer_field_mapping = {
        ndb.StringProperty: CharField,
        ndb.TextProperty: CharField,
        ndb.IntegerProperty: IntegerField,
        ndb.FloatProperty: FloatField,
        ndb.BooleanProperty: BooleanField,
        ndb.DateProperty: DateField,
        ndb.DateTimeProperty: fields.UTCDateTimeField,
    }
    serializer_related_field = fields.UrlsafeKeyPropertyField
    serializer_choice_field = ChoiceField

    # Default `create` and `update` behavior...

    def create(self, validated_data):
        ModelClass = self.Meta.model
        try:
            instance = ModelClass.create(enable_hook=True, **validated_data)
        except TypeError as exc:
            msg = (
                'Got a `TypeError` when calling `%s.create()`. '
                'This may be because you have a writable field on the '
                'serializer class that is not a valid argument to '
                '`%s.create()`. You may need to make the field '
                'read-only, or override the %s.create() method to handle '
                'this correctly.\nOriginal exception text was: %s.' %
                (
                    ModelClass.__name__,
                    ModelClass.__name__,
                    self.__class__.__name__,
                    exc
                )
            )
            raise TypeError(msg)
        return instance

    def update(self, instance, validated_data):
        instance.update(enable_hook=True, **validated_data)
        return instance

    # Determine the fields to apply...

    def get_fields(self):
        """
        Return the dict of field names -> field instances that should be
        used for `self.fields` when instantiating the serializer.
        """
        assert hasattr(self, 'Meta'), (
            'Class {serializer_class} missing "Meta" attribute'.format(
                serializer_class=self.__class__.__name__
            )
        )
        assert hasattr(self.Meta, 'model'), (
            'Class {serializer_class} missing "Meta.model" attribute'.format(
                serializer_class=self.__class__.__name__
            )
        )

        declared_fields = copy.deepcopy(self._declared_fields)
        model = getattr(self.Meta, 'model')
        depth = getattr(self.Meta, 'depth', 0)

        if depth is not None:
            assert depth >= 0, "'depth' may not be negative."
            assert depth <= 10, "'depth' may not be greater than 10."

        # Retrieve metadata about fields & relationships on the model class.
        info = model_meta.get_field_info(model)
        field_names = self.get_field_names(declared_fields, info)
        extra_kwargs = self.get_extra_kwargs()

        # Determine the fields that should be included on the serializer.
        fields = OrderedDict()

        for field_name in field_names:
            # If the field is explicitly declared on the class then use that.
            if field_name in declared_fields:
                fields[field_name] = declared_fields[field_name]
                continue

            # Determine the serializer field class and keyword arguments.
            field_class, field_kwargs = self.build_field(
                field_name, info, model, depth
            )

            # Include any kwargs defined in `Meta.extra_kwargs`
            extra_field_kwargs = extra_kwargs.get(field_name, {})
            field_kwargs = self.include_extra_kwargs(
                field_kwargs, extra_field_kwargs
            )

            # Create the serializer field.
            fields[field_name] = field_class(**field_kwargs)

        return fields

    # Methods for determining the set of field names to include...

    def get_field_names(self, declared_fields, info):
        """
        Returns the list of all field names that should be created when
        instantiating this serializer class. This is based on the default
        set of fields, but also takes into account the `Meta.fields` or
        `Meta.exclude` options if they have been specified.
        """
        fields = getattr(self.Meta, 'fields', None)
        exclude = getattr(self.Meta, 'exclude', None)

        if fields and not isinstance(fields, (list, tuple)):
            raise TypeError(
                'The `fields` option must be a list or tuple or "__all__". '
                'Got %s.' % type(fields).__name__
            )

        if exclude and not isinstance(exclude, (list, tuple)):
            raise TypeError(
                'The `exclude` option must be a list or tuple. Got %s.' %
                type(exclude).__name__
            )

        assert not (fields and exclude), (
            "Cannot set both 'fields' and 'exclude' options on "
            'serializer {serializer_class}.'.format(
                serializer_class=self.__class__.__name__
            )
        )

        if fields is None and exclude is None:
            warnings.warn(
                "Creating a ModelSerializer without either the 'fields' "
                "attribute or the 'exclude' attribute is pending deprecation "
                "since 3.3.0. Add an explicit fields = '__all__' to the "
                '{serializer_class} serializer.'.format(
                    serializer_class=self.__class__.__name__
                ),
                PendingDeprecationWarning
            )

        if fields is not None:
            # Ensure that all declared fields have also been included in the
            # `Meta.fields` option.

            # Do not require any fields that are declared a parent class,
            # in order to allow serializer subclasses to only include
            # a subset of fields.
            required_field_names = set(declared_fields)
            for cls in self.__class__.__bases__:
                required_field_names -= set(getattr(cls,
                                                    '_declared_fields', []))

            for field_name in required_field_names:
                assert field_name in fields, (
                    "The field '{field_name}' was declared on serializer "
                    '{serializer_class}, but has not been included in the '
                    "'fields' option.".format(
                        field_name=field_name,
                        serializer_class=self.__class__.__name__
                    )
                )
            return fields

        # Use the default set of field names if `Meta.fields` is not specified.
        fields = self.get_default_field_names(declared_fields, info)

        if exclude is not None:
            # If `Meta.exclude` is included, then remove those fields.
            for field_name in exclude:
                assert field_name in fields, (
                    "The field '{field_name}' was included on serializer "
                    "{serializer_class} in the 'exclude' option, but does "
                    'not match any model field.'.format(
                        field_name=field_name,
                        serializer_class=self.__class__.__name__
                    )
                )
                fields.remove(field_name)

        return fields

    def get_default_field_names(self, declared_fields, model_info):
        """
        Return the default list of field names that will be used if the
        `Meta.fields` option is not specified.
        """
        return (
            list(declared_fields.keys()) +
            list(model_info.fields.keys()) +
            list(model_info.forward_relations.keys())
        )

    def build_field(self, field_name, info, model_class, nested_depth):
        """
        Return a two tuple of (cls, kwargs) to build a serializer field with.
        """
        if field_name in info.fields_and_pk:
            model_field = info.fields_and_pk[field_name]
            return self.build_standard_field(field_name, model_field)
        elif field_name in info.relations:
            relation_info = info.relations[field_name]
            if not nested_depth:
                return self.build_relational_field(field_name, relation_info)
            else:
                return self.build_nested_field(
                    field_name, relation_info, nested_depth)
        elif hasattr(model_class, field_name):
            return self.build_property_field(field_name, model_class)
        return self.build_unknown_field(field_name, model_class)

    def build_standard_field(self, field_name, model_field):
        """
        Create regular model fields.
        """
        field_mapping = ClassLookupDict(self.serializer_field_mapping)

        field_class = field_mapping[model_field]
        field_kwargs = get_field_kwargs(field_name, model_field)

        if 'choices' in field_kwargs:
            # Fields with choices get coerced into `ChoiceField`
            # instead of using their regular typed field.
            field_class = self.serializer_choice_field

        if not issubclass(field_class, ModelField):
            # `model_field` is only valid for the fallback case of
            # `ModelField`, which is used when no other typed field
            # matched to the model field.
            field_kwargs.pop('model_field', None)

        if not issubclass(field_class, CharField) and not issubclass(field_class, ChoiceField):  # NOQA
            # `allow_blank` is only valid for textual fields.
            field_kwargs.pop('allow_blank', None)

        if model_field._repeated:
            field_kwargs.update(child=field_class(**field_kwargs))
            field_class = ListField

        return field_class, field_kwargs

    def build_property_field(self, field_name, model_class):
        """
        Create a read only field for model methods and properties.
        """
        field_class = ReadOnlyField
        field_kwargs = {}

        return field_class, field_kwargs

    def build_relational_field(self, field_name, relation_info):
        """
        Create fields for forward and reverse relationships.
        """
        field_class = self.serializer_related_field
        field_kwargs = {'read_only': True}

        return field_class, field_kwargs

    def build_nested_field(self, field_name, relation_info, nested_depth):
        """
        Create nested fields for forward and reverse relationships.
        """
        class NestedSerializer(ModelSerializer):

            class Meta:
                model = relation_info.related_model
                depth = nested_depth - 1

        field_class = NestedSerializer
        field_kwargs = get_nested_relation_kwargs(relation_info)

        return field_class, field_kwargs

    def build_unknown_field(self, field_name, model_class):
        """
        Raise an error on any unknown fields.
        """
        raise ImproperlyConfigured(
            'Field name `%s` is not valid for model `%s`.' %
            (field_name, model_class.__name__)
        )

    def include_extra_kwargs(self, kwargs, extra_kwargs):
        """
        Include any 'extra_kwargs' that have been included for this field,
        possibly removing any incompatible existing keyword arguments.
        """
        if extra_kwargs.get('read_only', False):
            for attr in [
                'required', 'default', 'allow_blank', 'allow_null',
                'min_length', 'max_length', 'min_value', 'max_value',
                'validators', 'queryset'
            ]:
                kwargs.pop(attr, None)

        if extra_kwargs.get('default') and kwargs.get('required') is False:
            kwargs.pop('required')

        if kwargs.get('read_only', False):
            extra_kwargs.pop('required', None)

        kwargs.update(extra_kwargs)

        return kwargs

    def get_extra_kwargs(self):
        """
        Return a dictionary mapping field names to a dictionary of
        additional keyword arguments.
        """
        extra_kwargs = getattr(self.Meta, 'extra_kwargs', {})

        read_only_fields = getattr(self.Meta, 'read_only_fields', None)
        if read_only_fields is not None:
            for field_name in read_only_fields:
                kwargs = extra_kwargs.get(field_name, {})
                kwargs['read_only'] = True
                extra_kwargs[field_name] = kwargs

        # These are all pending deprecation.
        write_only_fields = getattr(self.Meta, 'write_only_fields', None)
        if write_only_fields is not None:
            warnings.warn(
                'The `Meta.write_only_fields` option is deprecated. '
                "Use `Meta.extra_kwargs={<field_name>: {'write_only': True}}` instead.",  # NOQA
                DeprecationWarning,
                stacklevel=3
            )
            for field_name in write_only_fields:
                kwargs = extra_kwargs.get(field_name, {})
                kwargs['write_only'] = True
                extra_kwargs[field_name] = kwargs

        view_name = getattr(self.Meta, 'view_name', None)
        if view_name is not None:
            warnings.warn(
                'The `Meta.view_name` option is deprecated. '
                "Use `Meta.extra_kwargs={'url': {'view_name': ...}}` instead.",
                DeprecationWarning,
                stacklevel=3
            )
            kwargs = extra_kwargs.get(api_settings.URL_FIELD_NAME, {})
            kwargs['view_name'] = view_name
            extra_kwargs[api_settings.URL_FIELD_NAME] = kwargs

        lookup_field = getattr(self.Meta, 'lookup_field', None)
        if lookup_field is not None:
            warnings.warn(
                'The `Meta.lookup_field` option is deprecated. '
                "Use `Meta.extra_kwargs={'url': {'lookup_field': ...}}` instead.",  # NOQA
                DeprecationWarning,
                stacklevel=3
            )
            kwargs = extra_kwargs.get(api_settings.URL_FIELD_NAME, {})
            kwargs['lookup_field'] = lookup_field
            extra_kwargs[api_settings.URL_FIELD_NAME] = kwargs

        return extra_kwargs

    def get_validators(self):
        return []

    def to_representation(self, instance):
        ret = super(ModelSerializer, self).to_representation(instance)
        for key, value in ret.iteritems():
            handler = getattr(self, 'represent_%s' % key, None)
            if handler:
                ret[key] = handler(value)
        return ret


def get_field_kwargs(field_name, model_field):
    kwargs = {}
    kwargs['model_field'] = model_field
    return kwargs


class AdminModelSerializer(ModelSerializer):

    def update(self, instance, validated_data):
        for field in instance.reference_properties:
            # field has format '<name>_key' => we want to get the <name>
            key = field[:-4]
            if key in validated_data:
                data = validated_data.pop(key)
                obj = getattr(instance, key)
                self.update(obj, data)

        should_save = len(validated_data.keys()) > 0
        for field_name, field_class in instance._properties.items():
            if (isinstance(field_class, ndb.StructuredProperty) and
                    field_name in validated_data):
                data = validated_data.pop(field_name)
                obj = getattr(instance, field_name)
                for key, value in data.items():
                    setattr(obj, key, value)
        if should_save:
            instance.update(**validated_data)
        return instance
