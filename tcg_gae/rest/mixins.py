from collections import OrderedDict

from google.appengine.ext import ndb
from rest_framework.decorators import list_route
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework.fields import ChoiceField

from exceptions import ResourceNotFoundError

import views


class DynamicMethodViewMixin(object):

    def dispatch(self, request, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        request = self.initialize_request(request, *args, **kwargs)
        self.request = request
        self.headers = self.default_response_headers  # deprecate?

        try:
            if request.method.lower() in self.http_method_names:
                handler = getattr(
                    self, request.method.lower(),
                    self.http_method_not_allowed)
                handler_kwargs = getattr(handler, 'kwargs', {})
                if handler_kwargs:
                    for key, value in handler_kwargs.iteritems():
                        setattr(self, key, value)
            else:  # pragma: no cover
                handler = self.http_method_not_allowed

            self.initial(request, *args, **kwargs)
            response = handler(request, *args, **kwargs)

        except Exception as exc:
            response = self.handle_exception(exc)

        self.response = self.finalize_response(
            request, response, *args, **kwargs)
        return self.response


class URLSafeGetObjectMixin(object):

    def get_object_from_lookup(self, urlsafe, **kwargs):
        return ndb.Key(urlsafe=urlsafe).get()

    def get_parent_object_from_lookup(self, urlsafe, **kwargs):
        return ndb.Key(urlsafe=urlsafe).get()


class ModelGetObjectMixin(object):

    model_class = None
    parent_model_class = None

    def _to_int(self, value):
        try:
            return int(value)
        except:
            return value

    def get_object_from_lookup(self, value, **kwargs):
        return self.model_class.get_by_id(self._to_int(value), **kwargs)

    def get_parent_object_from_lookup(self, value, **kwargs):
        return self.parent_model_class.get_by_id(self._to_int(value), **kwargs)


class GetObjectMixin(object):

    def get_object(self, **kwargs):
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        look_up_value = self.kwargs[lookup_url_kwarg]
        obj = self.get_object_from_lookup(look_up_value, **kwargs)
        if not obj:
            raise ResourceNotFoundException
        self.check_object_permissions(self.request, obj)
        return obj

    def check_object_permissions(self, request, obj):
        for permission in self.get_permissions():
            if not permission.has_object_permission(request, self, obj):
                self.permission_denied(request, permission)


class GetParentObjectMixin(object):

    parent_permission_classes = []
    parent_lookup_field = 'parent_lookup_pk'

    def get_parent_object(self, **kwargs):
        look_up_value = self.kwargs[self.parent_lookup_field]
        obj = self.get_parent_object_from_lookup(look_up_value, **kwargs)
        if not obj:  # pragma: no cover
            raise ResourceNotFoundException
        self.check_parent_object_permissions(self.request, obj)
        return obj

    def get_parent_permissions(self):
        return [permission() for permission in self.parent_permission_classes]

    def check_parent_object_permissions(self, request, obj):  # noqa pragma: no cover
        for permission in self.get_parent_permissions():
            if not permission.has_object_permission(request, self, obj):
                self.permission_denied(request, permission)


class PermissionDeniedMixin(object):

    permission_denied_class = None

    def permission_denied(self, request, permission):
        raise self.permission_denied_class(
            'Permission denied: %s' % permission.__class__.__name__)


class AdminSearchMixin(object):

    @list_route(methods=['get', 'post'])
    def search(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)


class ModelListMixin(object):

    def list_by_queryset(self, request, queryset, callback=None):
        page = self.paginate_queryset(queryset)
        if callback:
            page = map(callback, page)
        serializer = self.get_serializer(page, many=True)
        return views.response_success(serializer.data)


class AdminSchemaMixin(object):

    def _get_fields(self, serializer):
        data = OrderedDict()

        fields = serializer.get_fields()
        for field_name, field in fields.items():
            if isinstance(field, Serializer):
                data[field_name] = self._get_fields(field)
            else:
                result = {
                    'type': field.__class__.__name__,
                    'read_only': field.read_only
                }
                if isinstance(field, ChoiceField):
                    result.update(choices=field.choices)
                data[field_name] = result

        return data

    @list_route(methods=['get'])
    def schema(self, request, *args, **kwargs):
        serializer = self.serializer_class()
        return views.response_success(self._get_fields(serializer))


class NDBQuerysetMixin(object):

    def get_queryset(self):
        queryset = self.queryset
        if queryset is None and self.model_class:
            queryset = self.model_class.objects.filter()
        return queryset


class RetrieveModelMixin(object):

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return views.response_success(serializer.data)


class UpdateModelMixin(object):

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return views.response_success(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class CreateModelMixin(object):

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return views.response_success(serializer.data)

    def perform_create(self, serializer):
        serializer.save()


class ListModelMixin(object):

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return views.response_success(serializer.data)
