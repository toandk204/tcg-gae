from rest_framework import exceptions


class ValidationError(exceptions.ValidationError):

    def __init__(self, message='', meta=None):
        self.detail = {
            'type': self.__class__.__name__,
            'message': message,
            'meta': meta or {}
        }


class InvalidSignatureError(ValidationError):
    pass


class InvalidAPIKeyError(ValidationError):
    pass


class ResourceAlreadyExistError(ValidationError):
    pass


class ResourceNotFoundError(ValidationError):
    pass


class PermissionDeniedError(ValidationError):
    pass
