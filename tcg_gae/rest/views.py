import traceback

from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework import serializers


# NOTE: This code is very trickly
# We want to customizie rest_framework error handling
# But because rest_framewor 3.1.3 is not desgined to customize it,
# so we need to monkey patching the function `get_validation_error_detail`
# by calling `money_patch_errors`
# We want to customizie rest_framework error handling
# But because rest_framewor 3.1.3 is not desgined to customize it,
# so we need to monkey patching the function `get_validation_error_detail`
# to change it
# This is not a good practice, and it should be removed in future when
# rest_framework change their design
def money_patch_errors():
    def validation_error_detail(exc):
        return {
            api_settings.NON_FIELD_ERRORS_KEY: exc.detail
        }
    serializers.__dict__['get_validation_error_detail'] = validation_error_detail  # noqa


def prepare_error(exc):
    return {
        'type': exc.__class__.__name__,
        'message': str(exc) or 'Unknown message',
        'metadata': getattr(exc, 'meta', {})
    }


def handle_error(exc, context):
    headers = {'Content-Type': 'application/json'}

    content = {
        'error': {
            api_settings.NON_FIELD_ERRORS_KEY: prepare_error(exc)
        }
    }

    return Response(
        content, headers=headers,
        status=getattr(exc, 'status_code', 402))


def next_url(request, next_cursor):
    path = request.path
    query_params = {k: v for k, v in request.query_params.iteritems()}
    query_params['cursor'] = next_cursor
    query = '&'.join('%s=%s' % (k, v) for k, v in query_params.iteritems())
    return '%s?%s' % (path, query)


def response_success(data, request=None, next_cursor=None):
    headers = {'Content-Type': 'application/json'}
    content = {'data': data}
    if request and next_cursor:
        content['pagination'] = {
            'next_url': next_url(request, next_cursor),
        }
    return Response(content, headers=headers)


def prepare_errors(errors):
    data = {}
    for key, value in errors.items():
        if isinstance(value, list):
            value = {
                'type': 'ValidationError',
                'meta': {},
                'message': value[0]
            }
        data[key] = value
    return data


def response_error(errors):
    headers = {'Content-Type': 'application/json'}
    content = {'error': prepare_errors(errors)}
    return Response(content, headers=headers, status=402)
