from rest_framework.viewsets import ViewSetMixin
from rest_framework.generics import GenericAPIView

import mixins
import exceptions


class ModelViewSet(
        mixins.DynamicMethodViewMixin,
        mixins.URLSafeGetObjectMixin,
        mixins.GetObjectMixin,
        mixins.GetParentObjectMixin,
        mixins.ModelListMixin,
        ViewSetMixin,
        GenericAPIView):

    permission_denied_class = exceptions.PermissionDeniedError


class AdminModelViewSet(
        mixins.AdminSchemaMixin,
        mixins.AdminSearchMixin,
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        mixins.CreateModelMixin,
        mixins.ListModelMixin,
        ModelViewSet):

    pass
