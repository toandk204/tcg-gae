import hmac
from hashlib import sha256

from rest_framework.authentication import BaseAuthentication


class SignatureAuthentication(BaseAuthentication):

    api_keys = {}
    api_key_exception_class = None
    api_sig_exception_class = None
    debug = False

    @classmethod
    def generate_sig(cls, endpoint, params, secret):
        sig = endpoint
        for key in sorted(params.keys()):
            sig += '|%s=%s' % (key, params[key])
        return hmac.new(secret, sig.encode('utf-8'), sha256).hexdigest()

    def authenticate(self, request):
        data = {}
        for k in request.query_params:
            data[k] = request.query_params[k]
        for k in request.data:
            data[k] = request.data[k]
        api_key = data.get('api_key')
        if api_key not in self.api_keys:
            raise self.api_key_exception_class('Invalid api_key')

        api_secret = self.api_keys[api_key]
        api_sig = data.pop('api_sig', None)
        expected_sig = self.generate_sig(request.path, data, api_secret)
        if api_sig != expected_sig:
            message = 'Invalid api_sig'
            if self.debug:
                message += expected_sig
            raise self.api_sig_exception_class(message)
        return (None, None)
