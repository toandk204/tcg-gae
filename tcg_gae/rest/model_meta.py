from google.appengine.ext import ndb

from collections import OrderedDict
from rest_framework.utils.model_meta import FieldInfo, RelationInfo


def get_field_info(model):
    opts = model._properties

    pk = None
    fields = _get_fields(opts)
    fields_and_pk = fields
    forward_relations = _get_forward_relationships(opts)
    reverse_relations = {}
    relationships = forward_relations

    return FieldInfo(
        pk, fields, forward_relations, reverse_relations,
        fields_and_pk, relationships)


def _get_fields(opts):
    fields = OrderedDict()
    for name, value in opts.items():
        is_key_property = isinstance(value, ndb.KeyProperty)
        is_related_field = name.endswith('_key') and is_key_property
        is_structure_field = isinstance(value, ndb.StructuredProperty)
        if not (is_related_field or is_structure_field):
            fields[name] = value
    return fields


def _get_forward_relationships(opts):
    forward_relations = OrderedDict()
    for name, value in opts.items():
        is_key_property = isinstance(value, ndb.KeyProperty)
        is_related_field = name.endswith('_key') and is_key_property
        is_structure_field = isinstance(value, ndb.StructuredProperty)
        if is_related_field:
            if value._kind:
                model = ndb.Model._lookup_model(value._kind)
            else:
                model = None
            forward_relations[name] = RelationInfo(
                model_field=name,
                related_model=model,
                to_many=False,
                to_field=None,
                has_through_model=False
            )
        elif is_structure_field:
            forward_relations[name] = RelationInfo(
                model_field=name,
                related_model=value._modelclass,
                to_many=False,
                to_field=None,
                has_through_model=False
            )
    return forward_relations
