import arrow

from rest_framework import fields
from google.appengine.ext import ndb

from exceptions import ValidationError


class UTCDateTimeField(fields.DateTimeField):

    def to_internal_value(self, value):
        result = super(UTCDateTimeField, self).to_internal_value(value)
        return arrow.get(result).to('UTC').datetime.replace(tzinfo=None)

    def to_representation(self, value):
        if not value.microsecond:
            value = value.replace(microsecond=1)
        return super(UTCDateTimeField, self).to_representation(value)


class UrlsafeKeyPropertyField(fields.CharField):

    def to_internal_value(self, value):
        try:
            return ndb.Key(urlsafe=value)
        except:
            raise ValidationError()

    def to_representation(self, value):
        return value.urlsafe()


class ImageURLField(fields.CharField):
    pass


class MoneyField(fields.CharField):
    pass
