from google.appengine.ext import ndb
from rest_framework.filters import BaseFilterBackend


class NDBFilterBackend(BaseFilterBackend):
    """ Allow cstool could easily to query into database using filter
    Filter includes 2 fields
    + `query` is list of items. Each items include 2 fields:
        - `operator: '=', '!=', '>', '>=', '<', '<=', 'AND', 'OR'
        - operand: List of object. Operand could also be a filter item
    + `order`: list of item. Each item includes 2 fields:
        - `name`: field name
        - `asc`: true or false
    """

    def get_filters(self, request):
        result = request.query_params.get('filter')
        if not result:
            result = request.data.get('filter')
        return result

    def get_op2(self, prop, item):
        op2 = item['operands'][1]
        if isinstance(prop, ndb.KeyProperty):
            return ndb.Key(urlsafe=op2)
        return op2

    def get_filter_node(self, item, model_class):
        operator = item['operator']
        if operator not in ['AND', 'OR', 'IN']:
            op1 = item['operands'][0]
            prop = model_class._properties[op1]
            op2 = self.get_op2(prop, item)
            return prop._comparison(operator, op2)
        elif operator == 'IN':
            op1 = item['operands'][0]
            prop = model_class._properties[op1]
            op2 = self.get_op2(prop, item)
            return prop._IN(op2)
        elif operator == 'AND':
            return ndb.AND(*[
                self.get_filter_node(sub_item, model_class)
                for sub_item in item['operands']])
        elif operator == 'OR':
            return ndb.OR(*[
                self.get_filter_node(sub_item, model_class)
                for sub_item in item['operands']])

    def get_filters_node(self, filters, model_class):
        if 'query' in filters:
            return ndb.AND(*[
                self.get_filter_node(item, model_class)
                for item in filters['query']])

    def get_order_node(self, item, model_class):
        field = item['name']
        prop = model_class._properties[field]
        if item['asc']:
            return prop
        else:
            return -prop

    def get_orders(self, filters, model_class):
        if 'orders' in filters:
            return [
                self.get_order_node(item, model_class)
                for item in filters['orders']]

    def filter_queryset(self, request, queryset, view):
        filters = self.get_filters(request)
        if not filters:
            return queryset

        query = queryset
        filter_node = self.get_filters_node(filters, view.model_class)
        if filter_node:
            query = query.filter(filter_node)

        orders = self.get_orders(filters, view.model_class)
        if orders:
            query = query.order(*orders)

        return query
