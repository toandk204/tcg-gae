def kwargs(**data):
    def decorator(func):
        if not hasattr(func, 'kwargs'):
            func.kwargs = {}
        func.kwargs.update(**data)
        return func
    return decorator


def parent_permission_classes(permission_classes):
    return kwargs(parent_permission_classes=permission_classes)


def permission_classes(permission_classes):
    return kwargs(permission_classes=permission_classes)
