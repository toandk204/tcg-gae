from django.conf import settings
import sendgrid


class SendGridClient(object):

    client = sendgrid.SendGridClient(settings.SENDGRID_API_KEY)

    def send(self, email_to, subject, content):
        message = sendgrid.Mail()
        message.add_to(email_to)
        message.set_from(settings.SENDGRID_EMAIL_FROM)
        message.set_subject(subject)
        message.set_html(content)
        self.client.send(message)
