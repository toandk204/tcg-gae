import urllib
from google.appengine.api import urlfetch
from django.conf import settings


class NexmoClient(object):

    BASE_URL = 'https://rest.nexmo.com'

    def request(self, method, path, data=None):
        url = '%s/%s?api_key=%s&api_secret=%s' % (
            self.BASE_URL, path,
            settings.NEXMO_API_KEY, settings.NEXMO_API_SECRET)
        res = urlfetch.Fetch(
            url,
            payload=urllib.urlencode(data),
            method=method)
        return res

    def send(self, phone, message):
        self.request(
            urlfetch.POST, 'sms/json', {
                'to': phone,
                'text': message,
                'from': settings.NEXMO_BRAND_NAME
            })
