import factory
from gaendb import factories
from tcg_gae import utils


class KeyAttribute(factories.KeyAttribute):

    def __init__(self, kind_factory, _parent=None, _id=None, type=str):
        super(KeyAttribute, self).__init__(kind_factory, _parent, _id, type)


class NDBFactoryMetaClass(factories.NDBFactoryMetaClass):

    def __new__(meta_cls, class_name, bases, attrs):
        new_cls = super(NDBFactoryMetaClass, meta_cls).__new__(
            meta_cls, class_name, bases, attrs)
        # these SelfAttributes allow us to specify `id` and `parent` for
        # key when generating the instance
        key_attr = KeyAttribute(
            new_cls,
            _parent=factory.SelfAttribute('parent', default=None),
            _id=factory.SelfAttribute('id'),
            type=str,
        )
        new_cls.key = key_attr
        new_cls._meta.declarations['key'] = key_attr

        key_id_attr = factory.Sequence(lambda seq: seq, type=str)
        new_cls.id = key_id_attr
        new_cls._meta.declarations['id'] = key_id_attr
        if 'id' not in new_cls._meta.exclude:  # pragma: no cover
            new_cls._meta.exclude += ('id',)

        new_cls.parent = None
        new_cls._meta.declarations['parent'] = None
        if 'parent' not in new_cls._meta.exclude:  # pragma: no cover
            new_cls._meta.exclude += ('parent',)

        return new_cls


class NDBBaseFactory(factories.NDBBaseFactory):
    """
    BaseFactory for GAE NDB models in which key is uuid
    """

    @classmethod
    def _generate_next_sequence(cls):
        return utils.uuid()


NDBFactory = NDBFactoryMetaClass('NDBFactory', (NDBBaseFactory,), {
    'Meta': factories.NDBBaseMeta,
    '__doc__': """Factory base with build and create support.

    Extended to support GAE NDB Model.
    """,
})
