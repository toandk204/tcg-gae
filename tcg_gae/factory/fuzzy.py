import datetime

from factory import fuzzy


class FuzzyDeltaDateTime(fuzzy.BaseFuzzyAttribute):

    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def fuzz(self):
        return datetime.datetime.utcnow() + datetime.timedelta(**self.kwargs)
