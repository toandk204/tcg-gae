import mixins
from meta import ObjectSchema, MetaTestCase, BaseTestCase


class TestCase(
        mixins.GAETestCaseMixin,
        mixins.AssertObjectMixin,
        BaseTestCase):

    __metaclass__ = MetaTestCase
