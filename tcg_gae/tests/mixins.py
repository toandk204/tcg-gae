import json

from google.appengine.ext import testbed
from google.appengine.datastore import datastore_stub_util


class GAETestCaseMixin(object):

    # gae_queue_root_path define path which contains queue.yaml file
    gae_queue_root_path = ''

    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.setup_env(current_version_id='testbed.version')
        self.testbed.activate()
        self.policy = datastore_stub_util.PseudoRandomHRConsistencyPolicy(
            probability=1)
        self.testbed.init_datastore_v3_stub(consistency_policy=self.policy)
        self.testbed.init_memcache_stub()
        self.testbed.init_taskqueue_stub(
            root_path=self.gae_queue_root_path)
        self.taskqueue_stub = self.testbed.get_stub(
            testbed.TASKQUEUE_SERVICE_NAME)
        if hasattr(self, 'setUpModel'):
            self.setUpModel()
        if hasattr(self, 'setUpTest'):
            self.setUpTest()

    def tearDown(self):
        self.testbed.deactivate()


class AssertObjectMixin(object):

    def is_response_empty(self, res):
        self.assertEquals(200, res.status_code)
        data = json.loads(res.content)['data']
        self.assertEquals({}, data)
        return data

    def assert_object(self, data, schema):
        for field in schema.fields:
            self.assertIn(field, data)
        for name in schema.nested_schemas:
            nested_schema = self._schemas[name]
            self.assertIn(name, data)
            self.assert_object(data[name], nested_schema)

    def assert_response_object(self, res, schema):
        self.assertEquals(200, res.status_code)
        data = json.loads(res.content)['data']
        self.assert_object(data, schema)
        return data

    def assert_object_list(self, data, schema, size):
        self.assertEquals(size, len(data))
        for item in data:
            self.assert_object(item, schema)

    def assert_response_object_list(self, res, schema, size):
        self.assertEquals(200, res.status_code)
        data = json.loads(res.content)['data']
        self.assert_object_list(data, schema, size)
        return data

    def assert_error(self, fields, errors):
        for field in fields:
            field_name, error_type = field
            self.assertIn(field_name, errors)
            self.assertEquals(error_type, errors[field_name]['type'])

    def assert_response_error(self, fields, res):
        self.assertEquals(402, res.status_code)
        error = json.loads(res.content)['error']
        self.assert_error(fields, error)


class ViewTestCaseMixin(object):
    view_classes = []
    old_classes = {}

    @classmethod
    def setUpClass(self):
        from rest_framework.test import APIClient
        self.client = APIClient()
        for klass in self.view_classes:
            self.old_classes[klass] = klass.authentication_classes
            klass.authentication_classes = []

    @classmethod
    def tearDownClass(self):
        for klass in self.view_classes:
            klass.authentication_classes = [self.old_classes[klass]]
