import unittest


class MetaTestCase(type):

    def __init__(cls, name, bases, classdict):
        super(MetaTestCase, cls).__init__(name, bases, classdict)
        cls._auto_create_methods(bases)


class ObjectSchema(object):

    def __init__(self, *args, **kwargs):
        self.fields = args
        self.nested_schemas = kwargs.get('nested_schemas', [])


def _is_object(schema):
    def wrapper(self, data):
        return self.assert_object(data, schema)
    return wrapper


def _is_response_object(schema):
    def wrapper(self, res):
        return self.assert_response_object(res, schema)
    return wrapper


def _is_object_list(schema):
    def wrapper(self, data, size):
        return self.assert_object_list(data, schema, size)
    return wrapper


def _is_response_object_list(schema):
    def wrapper(self, res, size):
        return self.assert_response_object_list(res, schema, size)
    return wrapper


class BaseTestCase(unittest.TestCase):

    @classmethod
    def _find_object_schemas(cls, bases):
        # get schema from object schema fields
        cls._schemas = {
            name: value
            for name, value in cls.__dict__.items()
            if isinstance(value, ObjectSchema)
        }

        # get schema from super classes
        for base in bases:
            cls._schemas.update(getattr(base, '_schemas', {}))

    @classmethod
    def _auto_create_is_object_methods(cls):
        for name, schema in cls._schemas.items():
            setattr(
                cls, 'is_%s' % name,
                _is_object(schema))
            setattr(
                cls, '_is_%s' % name,
                _is_object(schema))
            setattr(
                cls, 'is_%s_list' % name,
                _is_object_list(schema))
            setattr(
                cls, '_is_%s_list' % name,
                _is_object_list(schema))
            setattr(
                cls, 'is_response_%s' % name,
                _is_response_object(schema))
            setattr(
                cls, '_is_response_%s' % name,
                _is_response_object(schema))
            setattr(
                cls, 'is_response_%s_list' % name,
                _is_response_object_list(schema))
            setattr(
                cls, '_is_response_%s_list' % name,
                _is_response_object_list(schema))

    @classmethod
    def _auto_create_methods(cls, bases):
        cls._find_object_schemas(bases)
        cls._auto_create_is_object_methods()
