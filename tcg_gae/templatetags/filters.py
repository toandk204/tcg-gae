from django import template
from django.utils.translation import ugettext as _


register = template.Library()


def get_attribute(obj, attribute):
    if not attribute:
        return obj
    attributes = attribute.split('.')
    value = obj

    try:
        for key in attributes:
            value = getattr(value, key)
        return value
    except:  # pragma: no cover
        return None


def get_value(item, attribute=None, fallback_attribute=None):
    value = get_attribute(item, attribute)
    if value is None and fallback_attribute:
        value = get_attribute(item, fallback_attribute)
    return value


@register.filter
def list_actors(actors, attribute=None, fallback_attribute=None, max_size=3):
    if not actors:
        return ''

    size = len(actors)
    if size == 1:
        return get_value(actors[0], attribute)
    else:
        if size > max_size:
            first_part = ', '.join(
                get_value(item, attribute, fallback_attribute)
                for item in actors[:max_size])
            second_part = size - max_size
        else:
            first_part = ', '.join(
                get_value(item, attribute, fallback_attribute)
                for item in actors[:-1])
            second_part = get_value(actors[-1], attribute, fallback_attribute)
        return '%s %s %s' % (first_part, _('and'), second_part)
