.. highlight:: shell

============
Installation
============

At the command line::

    $ easy_install tcg-gae

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv tcg-gae
    $ pip install tcg-gae
