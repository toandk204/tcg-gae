from google.appengine.ext import ndb
from tcg_gae.tests import TestCase
from tcg_gae.models import Model, Manager


class UserManager(Manager):
    pass


class User(Model):

    class Meta:
        manager = UserManager

    phone = ndb.StringProperty()
    email = ndb.StringProperty()


class AccessToken(Model):

    class Meta:
        parent = 'user'

    user_key = ndb.KeyProperty(kind='User')


class TestMetaClass(TestCase):

    def test_check_manager(self):
        self.assertTrue(isinstance(User.objects, UserManager))

    def test_check_reference_properties(self):
        self.assertEquals([], User.reference_properties.keys())
        self.assertEquals(
            ['user_key'], AccessToken.reference_properties.keys())

    def test_check_parent(self):
        self.assertEquals('user', AccessToken._parent_field)


class TestManager(TestCase):

    def test_query(self):
        User.objects.filter(name='hello').get()


class TestModel(TestCase):

    def test_create(self):
        user = User.create(email='hello', phone='84982')
        self.assertEquals('hello', user.email)
        self.assertEquals('84982', user.phone)

    def test_update(self):
        user = User.create(email='hello', phone='84982')
        user.update(email='new_email')
        self.assertEquals('new_email', user.email)
        self.assertEquals('84982', user.phone)
        self.assertEquals({'email': 'new_email'}, user._new_data)

    def test_create_with_reference(self):
        user = User.create(email='hello', phone='84982')

        token = AccessToken.create(user=user)
        self.assertEquals('hello', token.user.email)
        self.assertEquals('84982', token.user.phone)
