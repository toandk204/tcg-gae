from google.appengine.ext import deferred
from tcg_gae.task import task, single_task
from tcg_gae.tests import TestCase


@task()
def task_func(name):
    return 'hello %s' % name


@single_task()
def single_task_func(name):
    return 'hello %s' % name


@task()
class TaskClass(object):

    def __init__(self, name):
        self.name = name

    def execute(self):
        return 'hello %s' % self.name


@single_task()
class SingleTaskClass(object):

    def __init__(self, name):
        self.name = name

    def execute(self):
        return 'hello %s' % self.name


class TaskDecoratorTest(TestCase):

    def test_class_execute(self):
        TaskClass.delay('world')
        tasks = self.taskqueue_stub.get_filtered_tasks()
        result = deferred.run(tasks[0].payload)
        self.assertEquals('hello world', result)

    def test_func_execute(self):
        task_func.delay('world')
        tasks = self.taskqueue_stub.get_filtered_tasks()
        result = deferred.run(tasks[0].payload)
        self.assertEquals('hello world', result)

    def test_single_task_func(self):
        single_task_func.delay('world', key='foo')
        tasks = self.taskqueue_stub.get_filtered_tasks()
        deferred.run(tasks[0].payload)

    def test_single_task_class(self):
        SingleTaskClass.delay('world', key='foo')
        tasks = self.taskqueue_stub.get_filtered_tasks()
        deferred.run(tasks[0].payload)
