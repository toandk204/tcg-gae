import mock
import unittest

from tcg_gae.decorators import lazy


class SampleClass(object):

    def _calculate_foo(self):
        return 'hello'

    @lazy
    def foo(self):
        return self._calculate_foo()


class TestLazyDecorators(unittest.TestCase):

    def test_get(self):
        a = SampleClass()
        self.assertEquals('hello', a.foo)

        with mock.patch.object(SampleClass, '_calculate_foo') as mock_foo:
            a.foo
            self.assertEquals(0, mock_foo.call_count)

    def test_set(self):
        a = SampleClass()
        self.assertEquals('hello', a.foo)
        a.foo = 'well'
        self.assertEquals('well', a.foo)
