from google.appengine.ext import ndb
from gaendb.factories import NDBFactory
from tcg_gae.models import Model
from tcg_gae.factory import fuzzy
from tcg_gae.tests import TestCase


class Car(Model):
    created_at = ndb.DateTimeProperty()


class CarFactory(NDBFactory):

    class Meta:
        model = Car

    created_at = fuzzy.FuzzyDeltaDateTime(hours=1)


class TestFuzzyDeltaDateTime(TestCase):

    def test_fuzz(self):
        obj = CarFactory.create()
        self.assertTrue(obj.created_at)
