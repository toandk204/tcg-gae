===============================
tcggae
===============================

.. image:: https://img.shields.io/pypi/v/tcg-gae.svg
        :target: https://pypi.python.org/pypi/tcg-gae

.. image:: https://img.shields.io/travis/trustcircleglobal/tcg-gae.svg
        :target: https://travis-ci.org/trustcircleglobal/tcg-gae

.. image:: https://readthedocs.org/projects/tcg-gae/badge/?version=latest
        :target: https://readthedocs.org/projects/tcg-gae/?badge=latest
        :alt: Documentation Status


Python Boilerplate contains all the boilerplate you need to create a Python package.

* Free software: ISC license
* Documentation: https://tcg-gae.readthedocs.org.

Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
