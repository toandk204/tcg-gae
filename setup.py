#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

def get_packages(package):
    """
    Return root package and all sub-packages.
    """
    return [dirpath
            for dirpath, dirnames, filenames in os.walk(package)
            if os.path.exists(os.path.join(dirpath, '__init__.py'))]


setup(
    name='tcg-gae',
    version='0.1.0',
    description="Python Boilerplate contains all the boilerplate you need to create a Python package.",
    author="Kien Nguyen Trung",
    author_email='kien.nguyen@trustcircleglobal.com',
    url='https://github.com/trustcircleglobal/tcg-gae',
    packages=get_packages('tcg_gae'),
    package_dir={'tcg-gae': 'tcg_gae'},
    include_package_data=True,
    install_requires=[],
    license="ISCL",
    zip_safe=False,
    keywords='tcg-gae',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: ISC License (ISCL)',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='tests',
    tests_require=[]
)
